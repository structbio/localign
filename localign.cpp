#include <algorithm>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <tclap/CmdLine.h>
#include <vector>

struct atom{
  int serial;                     /*!< serial (progressive) number */
  std::string id;                 /*!< atom identifier (CA, CB, ...) */
  char altLoc;                    /*!< alternative location */
  std::string resName;            /*!< residue name */
  char chainId;                   /*!< chain identifier */
  int resSeq;                     /*!< residue number */
  char iCode;                     /*!< code for insertion of residues */
  Eigen::Vector3d coordinate;     /*!< coordinate */
  double occupancy;               /*!< occupancy */
  double tempFactor;              /*!< temperature factor */
  std::string element;            /*!< element type (carbon, oxygen, ...) */
  double charge;                  /*!< charge */
};

std::ostream& operator << ( std::ostream& out, atom& atm ){
  std::set<std::string> aa_list = {"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL","HIE"};
  if( aa_list.find(atm.resName)!=aa_list.end() ){
    out << "ATOM  ";
  }
  else{
    out << "HETATM";
  }
  out << std::setw(5) << atm.serial;
  out << " ";
  out << std::setw(4) << atm.id;
  out << atm.altLoc;
  out << std::setw(3) << atm.resName;
  out << " ";
  out << atm.chainId;
  out << std::setw(4) << atm.resSeq;
  out << atm.iCode;
  out << "   ";
  out << std::fixed << std::setprecision(3);
  out << std::setw(8) << atm.coordinate(0);
  out << std::setw(8) << atm.coordinate(1);
  out << std::setw(8) << atm.coordinate(2);
  out << std::fixed << std::setprecision(2);
  out << std::setw(6) << atm.occupancy;
  out << std::setw(6) << atm.tempFactor;
  out << "          "; // blank
  out << atm.element;
  if( atm.charge == 0. )
    out << "  ";
  else{
    out << std::setprecision(0) << fabs(atm.charge);
    if( atm.charge > 0 )
      out << '+';
    else if(atm.charge < 0){
      out << '-';
    }
  }
  out << std::endl;
  return out;
};

atom parseAtom(std::string& line){
  atom info;
  auto line_id = line.substr(0,6);
  if(line_id == std::string("ATOM  ") or line_id == std::string("HETATM") ){
    info.serial = atoi(line.substr(6, 5).c_str()); 
    info.id = line.substr(12, 4);
    info.altLoc = line.substr(16, 1).c_str()[0];
    info.resName = line.substr(17, 3);
    info.chainId = isdigit( line.substr(21, 1).c_str()[0] ) ? char( 'A'+atoi(line.substr(21, 1).c_str()) ) : line.substr(21, 1).c_str()[0];
    if(info.chainId == ' ') info.chainId = 'A';
    info.chainId = toupper(info.chainId);
    info.resSeq = atoi( line.substr(22, 4).c_str() );
    info.iCode = line.substr(26, 1).c_str()[0];
    info.coordinate = Eigen::Vector3d( atof(line.substr(30, 8).c_str()), 
                                       atof(line.substr(38, 8).c_str()),
                                       atof(line.substr(46, 8).c_str()));
  
    try{
      info.occupancy = atof(line.substr(54, 6).c_str()) ? atof(line.substr(54, 6).c_str()) : 0.;
    }
    catch(std::out_of_range& oor){
      info.occupancy = 0.0;
    }
  
    try{
      info.tempFactor = atof(line.substr(60, 6).c_str()) ? atof(line.substr(60, 6).c_str()) : 0.;
    }
    catch (std::out_of_range& oor){
      info.tempFactor = 0.0;
    }
    try{
      info.element = line.substr(76,2);
    }
    catch (std::out_of_range& oor){
      info.element = " H";
    }
    info.charge = 0;
    try {
      if( isdigit( line.substr(78, 2).c_str()[0] ) and ( line.substr(78, 2).c_str()[1]=='-' or line.substr(78, 2).c_str()[1]=='+') ){
        info.charge = line.substr(78, 2).c_str()[1]=='-' ? -atof(line.substr(78, 1).c_str()) : atof(line.substr(78, 1).c_str());
      }
    }
    catch (std::out_of_range& oor){
      info.charge = 0;
    }
  }
  return info;
};

// Kabsch algorithm
Eigen::Affine3d Find3DAffineTransform(Eigen::Matrix3Xd in, Eigen::Matrix3Xd out) {

  // Default output
  Eigen::Affine3d A;
  A.linear() = Eigen::Matrix3d::Identity(3, 3);
  A.translation() = Eigen::Vector3d::Zero();

  if (in.cols() != out.cols())
    throw "Find3DAffineTransform(): input data mis-match";

  // First find the scale, by finding the ratio of sums of some distances,
  // then bring the datasets to the same scale.
  double dist_in = 0, dist_out = 0;
  for (int col = 0; col < in.cols()-1; col++) {
    dist_in  += (in.col(col+1) - in.col(col)).norm();
    dist_out += (out.col(col+1) - out.col(col)).norm();
  }
  if (dist_in <= 0 || dist_out <= 0)
    return A;
  double scale = dist_out/dist_in;
  out /= scale;

  // Find the centroids then shift to the origin
  Eigen::Vector3d in_ctr = Eigen::Vector3d::Zero();
  Eigen::Vector3d out_ctr = Eigen::Vector3d::Zero();
  for (int col = 0; col < in.cols(); col++) {
    in_ctr  += in.col(col);
    out_ctr += out.col(col);
  }
  in_ctr /= in.cols();
  out_ctr /= out.cols();
  for (int col = 0; col < in.cols(); col++) {
    in.col(col)  -= in_ctr;
    out.col(col) -= out_ctr;
  }

  // SVD
  Eigen::MatrixXd Cov = in * out.transpose();
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(Cov, Eigen::ComputeThinU | Eigen::ComputeThinV);

  // Find the rotation
  double d = (svd.matrixV() * svd.matrixU().transpose()).determinant();
  if (d >= 0)
    d = 1.0;
  else
    d = -1.0;
  Eigen::Matrix3d I = Eigen::Matrix3d::Identity(3, 3);
  I(2, 2) = d;
  Eigen::Matrix3d R = svd.matrixV() * I * svd.matrixU().transpose();

  // The final transform
  A.linear() = scale * R;
  A.translation() = scale*(out_ctr - R*in_ctr);

  return A;
}

double distance(std::pair<Eigen::Vector3d,Eigen::Quaternion<double>>& trans1, std::pair<Eigen::Vector3d,Eigen::Quaternion<double>>& trans2 ){
  double transl2 = (trans1.first-trans2.first).squaredNorm();
  auto q1 = trans1.second; q1.normalize();
  auto q2 = trans2.second; q2.normalize();
  auto scalar2 = q1.x()*q2.x()+q1.z()*q2.z()+q1.y()*q2.y()+q1.w()*q2.w();
  scalar2*=scalar2;
  double orient2 = 100.*acos(2*scalar2 -1);
  orient2*=orient2;
  return sqrt(transl2+orient2);
}

double smoothstep(double lower, double upper, double x){
    // Scale, bias and saturate x to 0..1 range
    x = std::max(0., std::min((x - lower)/(upper - lower),1.)); 
    // Evaluate polynomial
    return 1.-x*x*(3 - 2*x);
}

void normalize_matrix(Eigen::MatrixXd & m){
  uint ncols=m.cols();
  for(uint i=0; i!=ncols; ++i){
    double n = m.col(i).sum();
    for(uint j=0; j!=ncols; ++j){
      m(j,i)/=n;
    }
  }
}

void inflate_matrix(Eigen::MatrixXd & m, double q){
  uint ncols=m.cols();
  uint nrows=m.rows();
  for(uint i=0; i!=ncols; ++i){
    for(uint j=0; j!=ncols; ++j){
      m(i,j) = pow( m(i,j), q);
    }
  }
}


int main(int argc, char* argv[]){
  std::string pdb_filename1 = "";
  std::string pdb_filename2 = "";
  std::string out_filename = "";
  uint window = 0;
  double q = 0.;
  double lo = 0.;
  double hi = 0.;
  uint minsize = 0;
  std::string cluster_filename = "";
  std::string parameter_filename = "";
  
  try{
    TCLAP::CmdLine cmd("Align two structures", ' ', "0.1.1");
    TCLAP::ValueArg<std::string> pdb1Arg(    "s","source",    "in: source pdb file",                           true, "source.pdb",     "string")      ; cmd.add( pdb1Arg );
    TCLAP::ValueArg<std::string> pdb2Arg(    "t","target",    "in: target pdb file",                           true, "target.pdb",     "string")      ; cmd.add( pdb2Arg );
    TCLAP::ValueArg<std::string> outArg(     "o","out",       "output: aligned pdb file",                      true, "aligned.pdb",    "string")      ; cmd.add( outArg );
    TCLAP::ValueArg<uint> windowArg(         "w","window",    "in: window width in number of amino-acids",     false, 3,               "unsigned int"); cmd.add( windowArg );
    TCLAP::ValueArg<double> inflationArg(    "p","inflation", "in: inflation parameter during clustering",     false, 1.1,             "double")      ; cmd.add( inflationArg );
    TCLAP::ValueArg<double> lowCntArg(       "l","lower_cnt", "in: lower distance range for cnt probability",  false, 5.0,             "double")      ; cmd.add( lowCntArg );
    TCLAP::ValueArg<double> highCntArg(      "g","higher_cnt","in: higher distance range for cnt probability", false, 45.0,            "double")      ; cmd.add( highCntArg );
    TCLAP::ValueArg<uint> minSizeArg(        "m","minsize",   "in: minimum cluster size",                      false, 7,               "unsigned int"); cmd.add( minSizeArg );
    TCLAP::ValueArg<std::string> clusterArg( "c","clusters",  "output: list of residues per cluster",          false, "cluster.dat",   "string")      ; cmd.add( clusterArg );
    TCLAP::ValueArg<std::string> kabschArg(  "k","rototrans", "output: list of roto-translation parameters",   false, "parameters.dat","string")      ; cmd.add( kabschArg );
    cmd.parse( argc, argv );

    pdb_filename1 = pdb1Arg.getValue();
    pdb_filename2 = pdb2Arg.getValue();
    out_filename = outArg.getValue();
    window = windowArg.getValue();
    q = inflationArg.getValue();
    lo = lowCntArg.getValue();
    hi = highCntArg.getValue();
    minsize = minSizeArg.getValue();
    cluster_filename = clusterArg.getValue();
    parameter_filename = kabschArg.getValue();
  }
  catch (TCLAP::ArgException &e){
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return 1;
  }

  std::string line;
  std::map< char,std::vector<atom> > system_s, system_t;
  std::set< std::pair<char,int> > ca_s, ca_t;
  std::set< char > chainId;
  
  // read pdb1
  std::map<std::pair<char,int>,Eigen::Vector3d> coordinate_s;
  std::ifstream pdb_file1(pdb_filename1);
  while(std::getline(pdb_file1, line)){
    auto line_id = line.substr(0,6);
    if(line_id == std::string("ATOM  ")){
      auto atm = parseAtom(line);
      if(system_s.find(atm.chainId)==system_s.end()){
        system_s[atm.chainId]={};
        chainId.insert(atm.chainId);
      }
      system_s[atm.chainId].push_back(atm);
      if(atm.id==" CA " or atm.id=="CA  " or atm.id=="  CA"){
        auto pair=std::make_pair(atm.chainId,atm.resSeq);
        ca_s.insert(pair);
        coordinate_s[pair] = atm.coordinate;
      }
    }
  }
  pdb_file1.close();
  
  // read pdb2
  std::map<std::pair<char,int>,Eigen::Vector3d> coordinate_t;
  std::ifstream pdb_file2(pdb_filename2);
  while(std::getline(pdb_file2, line)){
    auto line_id = line.substr(0,6);
    if(line_id == std::string("ATOM  ")){
      auto atm = parseAtom(line);
      if(system_t.find(atm.chainId)==system_t.end()){
        system_t[atm.chainId]={};
      }
      system_t[atm.chainId].push_back(atm);
      if(atm.id==" CA " or atm.id=="CA  " or atm.id=="  CA"){
        auto pair=std::make_pair(atm.chainId,atm.resSeq);
        ca_t.insert(pair);
        coordinate_t[pair] = atm.coordinate;
      }
    }
  }
  pdb_file2.close();
  
  // get list of common CAs
  std::map<char, std::vector<Eigen::Vector3d> > ca_common_s,ca_common_t;
  uint n_ca_s = ca_s.size();
  for(auto ca: ca_s){
    auto k = ca_t.find( ca );
    if( k!=ca_s.end() ){
      ca_common_s[ca.first].push_back(  coordinate_s[ca]  );
      ca_common_t[ca.first].push_back(  coordinate_t[ca]  );
    }
  }
  
  // transformation parameters will be stored here
  std::map<char,std::vector< std::pair<Eigen::Vector3d,Eigen::Quaternion<double>> >> transformation;
  for(auto ch:chainId){
    transformation[ch]={};
  }
  
  // align chains
  std::ofstream parameter_file;
  parameter_file.open (parameter_filename.c_str());
  Eigen::Matrix3Xd in(3, window), out(3, window);
  for(auto ch: chainId){
    uint size=ca_common_s[ch].size();
    uint last=size-window+1;
    for(uint aa=0; aa!=last; ++aa){
      for(uint i=0; i!=window;++i){
        in.col(i) =ca_common_s[ch][aa+i];
        out.col(i)=ca_common_t[ch][aa+i];
      } 
      Eigen::Affine3d A = Find3DAffineTransform(in, out);
      auto translation = A.translation();
      auto quaternion = Eigen::Quaternion<double>(A.rotation());
      quaternion.normalize();
      transformation[ch].push_back( std::make_pair(translation,quaternion) );
      parameter_file << ch << " " << aa << " " << translation.transpose() << " " << quaternion.vec().transpose() << " " << quaternion.w() << std::endl;
    }
  }
  
  // create transformation index
  std::map< int,std::pair<char,int> > id2descr;
  std::map< std::pair<char,int>,int > descr2id;
  int N=0;
  for(auto ch:transformation){
    uint size = ch.second.size();
    for(uint i=0; i!=size; ++i){
      std::pair<char,int> descr = std::make_pair(ch.first,i);
      id2descr[N]=descr;
      descr2id[descr]=N;
      ++N;
    }
  }
  
  // compute transition probability matrix
  Eigen::MatrixXd transition = Eigen::MatrixXd::Zero(N,N);
  for(uint i=0; i!=(N-1); ++i){
    transition(i,i) = 1;
    for(uint j=i+1; j!=N; ++j){
      auto descr_i = id2descr[i];
      auto descr_j = id2descr[j];
      transition(i,j)=distance( transformation[descr_i.first][descr_i.second],transformation[descr_j.first][descr_j.second] );
      transition(i,j)=smoothstep( 0., hi, transition(i,j) );
      transition(j,i)=transition(i,j);
    }
  }
  normalize_matrix(transition);
  
  // implement mcl
  Eigen::MatrixXd transition_last = Eigen::MatrixXd::Zero(N,N);
  double convergence_test = 1.e-12*N*N;
  while( (transition-transition_last).norm() > convergence_test ){
    transition_last=transition;
    transition*=transition;
    inflate_matrix(transition,q);
    normalize_matrix(transition);
  }
  
  // find all clusters
  std::vector< std::set<int> > cluster;
  for(uint i=0; i!=N; ++i){
    if( transition.row(i).sum()<N*1.e-9 ){
      continue;
    }
    cluster.push_back({});
    for(uint j=0; j!=N; ++j){
      if( transition(i,j)>1.e-8 ){
        cluster.back().insert( j );
      }
    }
  }
  
  // filter relevant clustes
  std::ofstream cluster_file;
  cluster_file.open(cluster_filename.c_str());
  cluster_file << "#Warning! residue id in this file has been renamed in such a way that res X1 is the first residue of chain X that is present in both structures" << std::endl;
  std::set<uint> relevant_cluster;
  uint k=0;
  for(auto cl: cluster){
    uint s = cl.size();
    if(s>minsize){
      relevant_cluster.insert(k);
      cluster_file << "cluster_id: " << k << " cluster_size: " << s << " CA_list: ";
      for(auto t: cl){
        auto descr = id2descr[t];
        cluster_file << descr.first << "" << descr.second+1 << " ";
      } 
      cluster_file << std::endl;
    }
    ++k;
  }
  cluster_file.close();

  // align structure by using relevant clusters
  uint window_center = floor(window/2.)+1;
  for( auto cl_id: relevant_cluster ){
    uint n_atoms=cluster[cl_id].size();
    Eigen::Matrix3Xd in(3, n_atoms), out(3, n_atoms);
    uint k=0;
    for(auto atm_id: cluster[cl_id]){
      std::pair<char,int> descr = id2descr[atm_id];
      in.col(k)=ca_common_s[descr.first][descr.second+window_center];
      out.col(k)=ca_common_t[descr.first][descr.second+window_center];
      ++k;
    }
    Eigen::Affine3d kabsch = Find3DAffineTransform(in, out);
    auto translation = kabsch.translation();
    auto quaternion = Eigen::Quaternion<double>(kabsch.rotation());
    quaternion.normalize();
    parameter_file << "cl:" << " " << cl_id << " " << translation.transpose() << " " << quaternion.vec().transpose() << " " << quaternion.w() << std::endl;
    
    // print output pdb
    std::string aligned_filename = "cl_"+std::to_string(cl_id)+"_"+out_filename;
    std::ofstream out_file;
    out_file.open(aligned_filename.c_str());
    auto system = system_s;
    for(auto ch:chainId ){
      for(auto atm:system[ch]){
        atm.coordinate = kabsch*atm.coordinate;
        out_file << atm;
      }
    }
    out_file.close();
  }
  parameter_file.close();
  
  return 0;
}
