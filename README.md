# localign vers. 0.1.1 #
Align rigid domains of two protein structures

## How it works ##
localign will:
1. read one source and one target structure in **pdb** format
2. select residues which are common between the two
3. find the best roto-translation for corresponding windows of predefined width
4. cluster all roto-ranslations
5. find the best global rototranslation for every cluster
6. produce an aligned structure for every cluster

## Usage: ##

`localign  [-k <string>] [-c <string>] [-m <unsigned int>] [-g <double>] [-l <double>] [-p <double>] [-w <unsigned int>] -o <string> -t <string> -s <string> [--] [--version] [-h]`

Where: 

- -s <string>,  --source <string> (input file) source pdb file. **required**.
- -t <string>,  --target <string> (input file) target pdb file. **required**.
- -o <string>,  --out <string> (output structure basename) aligned pdb filename will be of the form 'cl_XX_basename'. **required**.
- -k <string>,  --rototrans <string> (output file) will contain a list of roto-translation parameters for every window and every cluster. Default: //parameters.dat//.
- -c <string>,  --clusters <string> (output file) will contain a list of all residues per cluster. Default: //cluster.dat//.
- -w <unsigned int>,  --window <unsigned int> (input parameter) window width in number of amino-acids. Default: //3//.
- -m <unsigned int>,  --minsize <unsigned int> (input parameter) minimum cluster size. Default: //7//.
- -g <double>,  --higher_cnt <double> (input parameter) higher distance range for cnt probability (see below). Default: //5//.
- -l <double>,  --lower_cnt <double> (input parameter) lower distance range for cnt probability (see below). Default: //45//.
- -p <double>,  --inflation <double> (input parameter) inflation parameter during markov clustering (see below). Default: //1.1//.
- --version  Displays version information and exits.
- -h,  --help  Displays usage information and exits.

## Clustering ##

The program uses [MCL](http://micans.org/mcl/) as clustering algorithm.
The Markov Matrix is computed as follows:
- for every pair of points i,j the distance is given by \sqrt(dr^2+100.*d\theta^2)
- this distance is then transformed to a probability with the smoothstep function: 1.-x\*x\*(3 - 2*x), where x is max(0., min((x - lower)/(upper - lower),1.)). //lower// and //upper// can be adjusted with the //-l// and //-g// commandline option, respectively.

During every step of MCL, the transition matrix is squared and then inflated with parameter **p**, tunable with the //-p// commandline option.

## Plot parameters.dat in gnuplot ##

Each line of parameters.dat begins with an identifier of the corresponding window or cluster. If it is necessary to higlight cluster parameters it is sufficient to filter cluster identifier ("cl:"):

`sp "<awk '$1!=\"cl:\"' parameters.dat" u 3:4:5, "<awk '$1==\"cl:\"' parameters.dat" u 3:4:5 pt 7 ps 1` for plotting translations

`sp "<awk '$1!=\"cl:\"' parameters.dat" u 6:7:8, "<awk '$1==\"cl:\"' parameters.dat" u 6:7:8 pt 7 ps 1` for plotting rotations

## Compilation ##
`g++ --std=c++11 --fast-math -msse3 -O3 localign.cpp -o localign`

**Dependencies**
- tclap: for option parsing
