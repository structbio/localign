= Run test =
`localign -s 4nu0.pdb -t 4ntz.pdb -o 4nu0.pdb`

== File description ==
**Input**
- //4nu0.pdb//: source structure, to be aligned to a target one.
- //4ntz.pdb//: target structure.

**Output**
- //cl_X_4nu0.pdb//: all possible different alignment (3 in this test case).
- //parameters.dat//: optimal roto-translations for every window and for every. 
- //cluster.dat//: list of residues belonging to each cluster.
